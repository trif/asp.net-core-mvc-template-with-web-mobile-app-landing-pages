﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Influearn.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult LandingApp()
        {
            return View();
        }

        public IActionResult LandingWeb()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
        
        [HttpGet("~/api/influearn")]
        public IActionResult influearnGet()
        {
            return Ok("ok");
        }
    }
}
